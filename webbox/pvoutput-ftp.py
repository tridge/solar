#!/usr/bin/env python
# post updated status of a PV system to pvoutput.org for a SMA webbox
# data is fetched via the SMA webbox ftp interface
# Copyright Andrew Tridgell 2010
# released under GNU GPL v3 or later

import os, sys, zipfile, glob, time, csv
from optparse import OptionParser

# parse command line options
parser = OptionParser()
parser.add_option("", "--webbox", help="webbox server name", default=None)
parser.add_option("", "--password", help="webbox user password", default=None)
parser.add_option("", "--apikey", help="pvoutput API key", default=None)
parser.add_option("", "--systemid", help="pvoutput system ID (an integer)", default=None)
parser.add_option("", "--url", help="PV output status URL",
                  default="http://pvoutput.org/service/r1/addstatus.jsp")
parser.add_option("", "--verbose", help="be more verbose", action='store_true', default=False)

(opts, args) = parser.parse_args()

if opts.systemid is None:
    print("You must provide a system ID")
    sys.exit(1)

if opts.apikey is None:
    print("You must provide an API key")
    sys.exit(1)

if opts.password is None:
    print("You must provide a password")
    sys.exit(1)

if opts.webbox is None:
    print("You must provide a webbox address")
    sys.exit(1)


def fetch_file(dirname, fname):
    '''get a file from a ftp server'''
    from ftplib import FTP
    f = open(fname, mode='w')

    if opts.verbose:
        print("Fetching %s from directory %s on %s" % (fname, dirname, opts.webbox))

    def callback(line):
        f.write(line)
        f.write('\n')
    
    ftp = FTP(host=opts.webbox, timeout=30)
    ftp.login(user='user', passwd=opts.password)
    ftp.cwd(dirname)
    ftp.retrlines('RETR ' + fname, callback=callback)
    f.close()


def post_status(date, time, energy, power):
    '''post status to pvoutput'''
    import httplib, urllib
    if opts.url[0:7] != "http://":
        print("URL must be of form http://host/service")
        sys.exit(1)
    url = opts.url[7:].split('/')
    host = url[0]
    service = '/' + '/'.join(url[1:])
    params = urllib.urlencode({ 'd' : date,
                                't' : time,
                                'v1' : energy * 1000,
                                'v2' : power})
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain",
               "X-Pvoutput-SystemId" : opts.systemid,
               "X-Pvoutput-Apikey" : opts.apikey}
    if opts.verbose:
        print("Connecting to %s" % host)
    conn = httplib.HTTPConnection(host)
    if opts.verbose:
        print("sending: %s" % params)
    conn.request("POST", service, params, headers)
    response = conn.getresponse()
    if response.status != 200:
        print "POST failed: ", response.status, response.reason, response.read()
        sys.exit(1)
    if opts.verbose:
        print "POST OK: ", response.status, response.reason, response.read()

def parse_csv(fname):
    '''parse a SMA webbox CSV file'''
    f = open(fname)

    if opts.verbose:
        print("Parsing CSV file %s" % fname)

    # work out the columns 
    header = f.readline().split(';')
    f.readline()
    models = f.readline().split(';')
    serial_numbers = f.readline().split(';')
    columns = f.readline().split(';')
    units = f.readline().split(';')
    mapped_columns = [columns[0]]
    counts = {}

    # remap column names to cope with duplicates (multiple inverters)
    for c in columns:
        counts[c] = 1
    for c in columns[1:]:
        mapped_columns.append("%s%u" % (c, counts[c]))
        counts[c] += 1

    def totals(row):
        '''calculate totals across all inverters'''
        counts = {}
        for c in range(1, len(columns)):
            if units[c] in ['W', 'kWh', 'A']:
                if not columns[c] in row:
                    row[columns[c]] = 0.0
                v = row[mapped_columns[c]]
                if v != '':
                    if not columns[c] in counts:
                        counts[columns[c]] = 0
                    row[columns[c]] += float(v)
                    counts[columns[c]] += 1
        for c in counts:
            row['%s_Count' % c] = counts[c]
        return row

    # parse it
    r = csv.DictReader(f, fieldnames=mapped_columns, delimiter=';')
    rows = []
    for row in r:
        t = totals(row)
        rows.append(t)
    return rows
    

# get todays CSV
csvname = time.strftime("%Y-%m-%d.csv")

# the webbox puts it in DATA/YEAR
data_dir = time.strftime("DATA/%Y")

date_str = time.strftime("%Y%m%d")

fetch_file(data_dir, csvname)
data = parse_csv(csvname)

# work out the number of inverters online
num_inverters = 1
for r in data:
    if data[-1]['E-Total_Count'] > num_inverters:
        num_inverters = data[-1]['E-Total_Count']

if opts.verbose:
    print("Found data for %u inverters" % num_inverters)

# work out the start of day value
for r in data:
    if r['E-Total_Count'] == num_inverters:
        first_total = r['E-Total']
        break

if opts.verbose:
    print("Power at start of day: %.2f" % first_total)

# grab just the last value
r = data[-1]
if r['E-Total_Count'] != num_inverters:
    if opts.verbose:
        print("Only %u inverters online" % r['E-Total_Count'])
    sys.exit(0)

post_status(date_str, r['TimeStamp'], r['E-Total'] - first_total, r['Pac'])
if opts.verbose:
    print("All OK")

