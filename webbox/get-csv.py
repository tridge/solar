#!/usr/bin/env python
# get todays CSV file from a SMA webbox
# Copyright Andrew Tridgell 2010
# released under GNU GPL v3 or later

import os, sys, time
from optparse import OptionParser

# parse command line options
parser = OptionParser()
parser.add_option("", "--webbox", help="webbox server name", default=None)
parser.add_option("", "--password", help="webbox user password", default=None)
parser.add_option("", "--verbose", help="be more verbose", action='store_true', default=False)

(opts, args) = parser.parse_args()

if opts.password is None:
    print("You must provide a password")
    sys.exit(1)

if opts.webbox is None:
    print("You must provide a webbox address")
    sys.exit(1)


def fetch_file(dirname, fname):
    '''get a file from a ftp server'''
    from ftplib import FTP
    f = open(fname, mode='w')

    if opts.verbose:
        print("Fetching %s from directory %s on %s" % (fname, dirname, opts.webbox))

    def callback(line):
        f.write(line)
        f.write('\n')
    
    ftp = FTP(host=opts.webbox, timeout=30)
    ftp.login(user='user', passwd=opts.password)
    ftp.cwd(dirname)
    ftp.retrlines('RETR ' + fname, callback=callback)
    f.close()

# get todays CSV
csvname = time.strftime("%Y-%m-%d.csv")

# the webbox puts it in DATA/YEAR
data_dir = time.strftime("DATA/%Y")

date_str = time.strftime("%Y%m%d")

fetch_file(data_dir, csvname)
