#!/usr/bin/env python
# reset the MPPT algorithm on a SMC5000A inverter via RPC
# Copyright Andrew Tridgell 2010
# released under GNU GPL v3 or later

import os, sys, time, socket, hashlib, random, json
from optparse import OptionParser

# parse command line options
parser = OptionParser()
parser.add_option("", "--webbox", help="webbox server name", default=None)
parser.add_option("", "--password", help="webbox user password", default=None)
parser.add_option("", "--verbose", help="be more verbose", action='store_true',
                  default=False)

(opts, args) = parser.parse_args()

if opts.password is None:
    print("You must provide a password")
    sys.exit(1)

if opts.webbox is None:
    print("You must provide a webbox address")
    sys.exit(1)

if len(args) < 1:
    print("You must specify a list of inverter keys to reset")
    sys.exit(1)

def rpc_bind():
    '''bind to the RPC port'''

    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    tries = 30
    while tries > 0:
        try:
            udp.bind(("0.0.0.0", 34268))
            break
        except:
            tries -= 1
            time.sleep(1)
            pass
    if tries == 0:
        print("Timed out binding to port 34268")
        sys.exit(1)
    udp.settimeout(10.0)
    udp.connect((opts.webbox, 34268))
    return udp


def rpc_call(udp, call):
    '''make a RPC call to the webbox'''

    call['passwd'] = hashlib.md5(opts.password).hexdigest()
    call['version'] = '1.0'
    call['format'] = "JSON"
    call["id"] = str(random.randint(1, 100000))

    params = json.dumps(call).encode("UTF-16")[2:]

    udp.send(params)
    try:
        d = udp.recv(102400)
    except socket.timeout:
        print("timed out talking to %s" % opts.webbox)
        sys.exit(1)
    r = json.loads(d.decode("UTF-16"))
    if int(r['id']) != int(call['id']):
        print("Bad ID received: %s != %s" % (r['id'], call['id']))
    return r


def reset_mppt(key):
    '''reset the MPPT on a specified inverter'''

    udp = rpc_bind()

    # stop the MPP on this inverter
    ret = rpc_call(udp, { "proc" : "SetParameter",
                          "params" : { "devices" : [ { 'key': key,
                                                       'channels' :
                                                       [ { 'meta' : 'Betriebsart',
                                                           'value' : 'Stop' }]}]}})
    print(ret['result']['devices'][0]['channels'][0])
    # and immediately restart
    ret = rpc_call(udp, { "proc" : "SetParameter",
                          "params" : { "devices" : [ { 'key': key,
                                                       'channels' :
                                                       [ { 'meta' : 'Betriebsart',
                                                           'value' : 'Mpp-Betrieb' }]}]}})
    print(ret['result']['devices'][0]['channels'][0])
    

for k in args:
    reset_mppt(k)

