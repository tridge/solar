This script will gather live data from your SMA webbox and publish the results
on www.pvoutput.org using the addstatus API

Assuming you run Linux or a similar unix-like OS, create a script
something like this, perhaps called 10min.sh:

  #!/bin/bash

  WEBBOX=your.webbox.hostname
  PASSWORD=XXXXXXX
  SYSTEMID=NNN
  APIKEY=xxxxxxxxxxxxxxxxxxxxxxxxx

  $HOME/pvoutput-rpc.py --password=$PASSWORD --webbox=$WEBBOX --systemid=$SYSTEMID --apikey=$APIKEY

and then setup cron to call this every 10 minutes during the day. A
crontab like this will do it:

*/10 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22 * * * $HOME/10min.sh

If you run Windows then maybe you could contribute a sample batch file
or similar which would have the same effect. The python code should
run OK on Windows (though I haven't tested it)

There are some other scripts in the directory as well that you may
find useful:

 pvoutput-rpc.py:  RPC based pvoutput updater
 get-csv.py:       fetch the daily CSV file
 pvoutput-ftp.py:  like pvoutput-rpc.py, but uses webbox ftp instead of RPC
 sma-data.py:      example of how to fetch detailed inverter data using RPC
