#!/usr/bin/env python
# post updated status of a PV system to pvoutput.org for a SMA webbox
# data is fetched via the SMA webbox RPC interface
# Copyright Andrew Tridgell 2010
# released under GNU GPL v3 or later

import os, sys, zipfile, glob, time, csv, json, hashlib, random, socket
import httplib, urllib
from optparse import OptionParser

# parse command line options
parser = OptionParser()
parser.add_option("", "--webbox", help="webbox server name", default=None)
parser.add_option("", "--password", help="webbox user password", default=None)
parser.add_option("", "--apikey", help="pvoutput API key", default=None)
parser.add_option("", "--systemid", help="pvoutput system ID (an integer)", default=None)
parser.add_option("", "--url", help="PV output status URL",
                  default="http://pvoutput.org/service/r1/addstatus.jsp")
parser.add_option("", "--verbose", help="be more verbose", action='store_true',
                  default=False)
parser.add_option("", "--dryrun", help="don't send to pvoutput", action='store_true',
                  default=False)

(opts, args) = parser.parse_args()

if opts.systemid is None:
    print("You must provide a system ID")
    sys.exit(1)

if opts.apikey is None:
    print("You must provide an API key")
    sys.exit(1)

if opts.password is None:
    print("You must provide a password")
    sys.exit(1)

if opts.webbox is None:
    print("You must provide a webbox address")
    sys.exit(1)

def rpc_call(call):
    '''make a RPC call to the webbox'''

    call['passwd'] = hashlib.md5(opts.password).hexdigest()
    call['version'] = '1.0'
    call['format'] = "JSON"
    call["id"] = str(random.randint(1, 100000))

    params = json.dumps(call).encode("UTF-16")[2:]

    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    tries = 30
    while tries > 0:
        try:
            udp.bind(("0.0.0.0", 34268))
            break
        except:
            tries -= 1
            time.sleep(1)
            pass
    if tries == 0:
        print("Timed out binding to port 34268")
        sys.exit(1)
        
    udp.settimeout(10.0)
    udp.connect((opts.webbox, 34268))
    udp.send(params)
    try:
        d = udp.recv(102400)
    except socket.timeout:
        print("timed out talking to %s" % opts.webbox)
        sys.exit(1)
    r = json.loads(d.decode("UTF-16"))
    if int(r['id']) != int(call['id']):
        print("Bad ID received: %s != %s" % (r['id'], call['id']))
    return r


def extract_result(v, name):
    res = {}
    if not 'result' in v:
        print("No result in data")
        sys.exit(1)
    r = v['result'][name]
    for n in r:
        try:
            res[n['name']] = float(n['value'])
        except:
            res[n['name']] = n['value']
    return res

def post_status(date, time, energy, power):
    '''post status to pvoutput'''
    if opts.url[0:7] != "http://":
        print("URL must be of form http://host/service")
        sys.exit(1)
    url = opts.url[7:].split('/')
    host = url[0]
    service = '/' + '/'.join(url[1:])
    params = urllib.urlencode({ 'd' : date,
                                't' : time,
                                'v1' : energy,
                                'v2' : power})
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain",
               "X-Pvoutput-SystemId" : opts.systemid,
               "X-Pvoutput-Apikey" : opts.apikey}
    if opts.verbose:
        print("Connecting to %s" % host)
    conn = httplib.HTTPConnection(host)
    if opts.verbose:
        print("sending: %s" % params)
    conn.request("POST", service, params, headers)
    response = conn.getresponse()
    if response.status != 200:
        print "POST failed: ", response.status, response.reason, response.read()
        sys.exit(1)
    if opts.verbose:
        print "POST OK: ", response.status, response.reason, response.read()

v = rpc_call({ "proc" : "GetPlantOverview" })

res = extract_result(v, 'overview')
power = res['GriPwr']
e_today = res['GriEgyTdy']*1000
date_str = time.strftime("%Y%m%d")
time_str = time.strftime("%H:%M")
if opts.dryrun:
    print(date_str, time_str, e_today, power)
else:
    if power > 0.0:
        post_status(date_str, time_str, e_today, power)
