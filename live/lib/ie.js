
/*
  IE doesn't have indexOf
 */
Array.prototype.indexOf = function(v) {
  for (var i=0; i<this.length; i++) {
    if (this[i] == v) {
      return i;
    }
  }
  return -1;
}
