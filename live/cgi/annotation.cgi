#!/usr/bin/env python

import cgi, sys, os, urllib

print('''Content-type: text/html

<html>
<head><title>annotations server - for automated use only</title></head>
<body>go away</body>
''')

allowed_addresses = [ "192.168.2.15", "127.0.0.1", "59.167.251.244" ]

addr = cgi.escape(os.environ["REMOTE_ADDR"])

if not addr in allowed_addresses:
    print("no access for :", addr)
    sys.exit(0)


form = cgi.FieldStorage()
series = form.getvalue("series")
xval = form.getvalue("xval")
text = form.getvalue("text")

if text is None:
    text = ''
    
if (series is None or xval is None):
    print("Bad form data")
    sys.exit(1)

series = series.replace(',','%2C')
text   = text.replace(',','%2C')

f = open("/var/www/solar.tridgell.net/html/CSV/annotations.csv", mode='a')
f.write('%u,"%s","%s"\n' % (long(xval), cgi.escape(series), cgi.escape(text)))
f.close()
